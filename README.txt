		README
	       ========
	 WEB SVN analysis Release
      -----------------------------

Description :
	Web SVN analysis is a web based application for analysing SVN diff files. Here end user can analyse lines of code change of a every user for every file format(like .java, .html, .js). Apart from that enduser can analysis code velocity and can visualize users actions in video format.

Download :

https://bitbucket.org/bbreddy/websvn

Requirements :

1) JDK(JDK 5+)
2) Server (Tomcat Server 7+)
3) Gource tool
4) ffmpeg

Installation Procedure :

1) Download complete project from given link ($ git clone https://bitbucket.org/bbreddy/websvn)
2) Generate .war file from ($ mvn compile war:war)
3) Deploy websvn.war file in server
4) Setup database
	Database Name : SVN
	Table Name : SVNLog
	Query for creating table : 
		CREATE TABLE 'SVNLog' (
	  	'revisionId' varchar(255) DEFAULT '',
		  'code' blob,
		  'numOfAddedLines' int(11) DEFAULT NULL,
		  'numOfDeletedLines' int(11) DEFAULT NULL,
		  'user' varchar(32) DEFAULT NULL,
		  'date' date DEFAULT '0000-00-00',
		  'mode' varchar(1) DEFAULT NULL,
		  'filepath' varchar(255) DEFAULT '',
		  'diffId' int(11) NOT NULL AUTO_INCREMENT,
		  PRIMARY KEY ('diffId'),
		  KEY 'userId' ('user')
		)
		
	CREATE TABLE SVNLog(
		revisionId varchar(255) DEFAULT '',
		code blob,
		numOfAddedLines int(11) DEFAULT NULL,
		numOfDeletedLines int(11) DEFAULT NULL,
		user varchar(32) DEFAULT NULL,
		date date DEFAULT '0000-00-00',
		mode varchar(1) DEFAULT NULL,
		filepath varchar(255) DEFAULT '',
		diffId int(11) NOT NULL AUTO_INCREMENT,
		 PRIMARY KEY (diffId),
		KEY userId (user));
		

5) Change database access credentials in hibernate.cfg.xml(WEB-INF/classes/hibernate.cfg.xml)
    
	   <property name="hibernate.connection.url">
	      jdbc:mysql://localhost:8080/SVN 
	   </property>
	   <property name="hibernate.connection.username">
	      USERNAME OF YOUR DATABASE
	   </property>
	   <property name="hibernate.connection.password">
	      PASSWORD OF YOUR DATABASE
	   </property>


6) Now you can access through  http://localhost:8080/websvn
