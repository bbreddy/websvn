<%@page import="org.cigital.websvn.velocity.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript" src="fusioncharts.js"></script>
<script type="text/javascript" src="fusioncharts.theme.fint.js"></script>
<script type="text/javascript">
<%String s1=request.getParameter("uname");
String s2=request.getParameter("fromdate");
String s3=request.getParameter("todate");
AvgAdded obj =new AvgAdded(s1,s2,s3);%>
FusionCharts.ready(function(){
    var revenueChart = new FusionCharts({
      "type": "column2d",
      "renderAt": "chartContainer2",
      "width": "400",
      "height": "300",
      "dataFormat": "json",
      "dataSource": {
        "chart": {
            "caption": "Avg Lines of Code Added per day",
            "xAxisName": "File Type",
            "yAxisName": "LOCC",
            "theme": "fint",
            "yAxisMaxValue":<%out.println(obj.max/obj.dayc);%>,
            
         },
        "data": [
            <%
            out.print(obj.result);%>
         ]
      }
  });

  revenueChart.render();
})
</script>
  <div id="chartContainer2"></div>
</body>
</html>
