<%@page import="org.cigital.websvn.velocity.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript" src="fusioncharts.js"></script>
<script type="text/javascript" src="fusioncharts.theme.fint.js"></script>
<script type="text/javascript">
<%String s1=request.getParameter("uname");
String s2=request.getParameter("fromdate");
String s3=request.getParameter("todate");
AvgAdded obj =new AvgAdded(s1,s2,s3);%>
FusionCharts.ready(function(){
    var revenueChart = new FusionCharts({
      "type": "column2d",
      "renderAt": "chartContainerl",
      "width": "400",
      "height": "300",
      "dataFormat": "json",
      "dataSource": {
        "chart": {
            "caption": "Avg Lines of Removed per day",
            "xAxisName": "File Type",
            "yAxisName": "LOCC",
            "yAxisMaxValue":<%out.println(obj.max/obj.dayc);%>,
            "theme": "fint",
            
         },
        "data": [
            <%
            out.print(obj.removed());%>
         ]
      }
  });

  revenueChart.render();
})
</script>
  <div id="chartContainerl"></div>
</body>
</html>
