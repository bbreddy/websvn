package org.cigital.websvn.codechange;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cigital.websvn.model.JqGridModel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JqGridServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		java.util.List<JqGridModel> jqGridModels = new ArrayList<JqGridModel>();
		for(int i=1;i<=100;i++){
			System.out.println("hi..");
			JqGridModel gridModel1 = new JqGridModel();
			gridModel1.setAuthorName("bala"+i);
			gridModel1.setCode("code"+i);
			gridModel1.setFileName("filename"+i);
			gridModel1.setRevNo("revno"+i);
			gridModel1.setTime("time"+i);
			gridModel1.setType("java"+i);
			((ArrayList<JqGridModel>) jqGridModels).add(gridModel1);
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(jqGridModels);
		jsonArray = "{\"page\":1,\"total\":\"10\",\"records\":" +jqGridModels.size() + ",\"rows\":" + jsonArray + "}";
		System.out.println(jsonArray);
		response.getWriter().print(jsonArray);
	}
}