package org.cigital.websvn.codechange;
/**
 * 
 * @author VINOD.K
 *
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Servlet implementation class hbs_servlet
 */
public class HbsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String username,fromdate,todate,rtype;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HbsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw=response.getWriter();
		try{
			username=request.getParameter("uname");
			fromdate=request.getParameter("fromdate");
			System.out.println(fromdate +" d1");
			String[] fd1=fromdate.split("/");
			fromdate=fd1[2]+fd1[0]+fd1[1];
			todate=request.getParameter("todate");
			rtype=request.getParameter("rtype");
			fd1=todate.split("/");
			todate=fd1[2]+fd1[0]+fd1[1];
			//new hbs_sample(pw,"ayesha","2015-05-01","2015-06-31");
			new HbsSample(pw,username,fromdate,todate);
		}
		catch(Exception e){
			pw.println(e.toString() +" hi123");
		}
	}

}
