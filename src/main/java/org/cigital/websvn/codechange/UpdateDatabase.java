package org.cigital.websvn.codechange;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.cigital.websvn.preprocessing.ParserToDB;
import org.hibernate.SessionFactory;


/**
 * Servlet implementation class UpdateDatabase
 */
public class UpdateDatabase extends HttpServlet {
	SessionFactory factory;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDatabase() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletFileUpload.isMultipartContent(request);
       // if(isMultipart){
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
        System.out.println(getServletContext().getRealPath("/"));
            try{
                List items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                while (iterator.hasNext()){
                    FileItem item = (FileItem) iterator.next();
                    if (!item.isFormField()){
                        String fileName = item.getName();
                        String root = getServletContext().getRealPath("/");
                        
                        File uploadedFile = new File(root,fileName);
                        String completePath=root+fileName;
                        item.write(uploadedFile);
                        System.out.println(completePath);
                        new ParserToDB().insertIntoDB(completePath);                       
                    }
                }
            }catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        //}
		// Implement parsing method
	}
}