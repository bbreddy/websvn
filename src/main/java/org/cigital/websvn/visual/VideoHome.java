package org.cigital.websvn.visual;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VideoHome
 */
public class VideoHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VideoHome() {
        super();
        // TODO Auto-generated constructor stub
    }
    	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		String fileName = request.getParameter("name");
		PrintWriter out=response.getWriter();
		out.println("<br><center><video height=480 width=640 controls><source src='"+fileName+".mp4' type='video/mp4'><source src='"+fileName+".webm' type='video/webm'></video></center><br>");
	}

}
