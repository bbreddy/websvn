package org.cigital.websvn.model;

public class Diff {
	private String filepath;
	private String code;
	Type action;	
	Filetype fname;
	public void setPath(String filepath){
		this.filepath=filepath;
		}
	public String getPath(){
		return filepath;
		}
	public void setCode(String code){
		this.code=code;
		}
	public String getCode(){
		return code;
		}
	public void setType(Type action)
	{
	this.action=action;
	}
	public Type getType( )
	{
	return action; //
	}
	
	public void setFiletype(Filetype fname)
	{
	this.fname=fname;
	}
	public Filetype getFiletype( )
	{
	return fname; //
	}

	public enum Type{
		add,update,delete;           
			}
	//for filetype
	public enum Filetype{
		html,xml,java,javascript,jsp,python,jquery,css;
	}
}