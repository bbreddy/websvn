package org.cigital.websvn.model;

import java.util.ArrayList;

public class Commit {
	private String refno;
	private String timestamp;
	private ArrayList<Diff> diff;
	public void setRefno(String refno)
	{
		this.refno=refno;
	}
	public String getRefno(){
		return refno;
	}

	public void setTimes(String timestamp)
	{
		this.timestamp=timestamp;
	}
	public String getTimes(){
		return timestamp;
	}

	public void setDifff(ArrayList<Diff> diff){
		this.diff=diff;
		}
	public ArrayList<Diff> getDifff(){
		return diff;
		}
}
