package org.cigital.websvn.velocity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cigital.websvn.model.SvnObject;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class avgLinesofcode
 */
public class AvgLinesOfCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String username,fromdate,todate,rtype;
	PrintWriter pw;
	SessionFactory factory;
	HashMap<String, Integer> linesAdded=new HashMap<String, Integer>();
	HashMap<String, Integer> linesRemoved=new HashMap<String, Integer>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AvgLinesOfCode() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		username=request.getParameter("uname");
		fromdate=request.getParameter("fromdate");
		System.out.println(fromdate +" d1");
		String[] fd1=fromdate.split("/");
		fromdate=fd1[2]+fd1[0]+fd1[1];
		todate=request.getParameter("todate");
		rtype=request.getParameter("rtype");
		fd1=todate.split("/");
		todate=fd1[2]+fd1[0]+fd1[1];
		factory=new Configuration().configure().buildSessionFactory();
		pw=response.getWriter();
		pw.println("gelo");
		addEmployee();
	}
	/* Method to CREATE a record in the database */
	   public Integer addEmployee(){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer employeeID = null;
	      try{
	         tx = session.beginTransaction();
	         List employees;
	         if(username.length()>0)
	        	 employees = session.createQuery("FROM SvnObject R WHERE R.user='"+username+"' and R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
	         else
	        	 employees = session.createQuery("FROM SvnObject R WHERE R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
	         for (Iterator iterator = 
                     employees.iterator(); iterator.hasNext();){
			  	 SvnObject diffo = (SvnObject) iterator.next();
			  	String type=diffo.getFilepath().substring(diffo.getFilepath().lastIndexOf('.')+1);
			  	 if(linesAdded.containsKey(type))
			  		 linesAdded.put(type,linesAdded.get(type)+diffo.getNumOfAddedLines());
			  	 else
			  		linesAdded.put(type,diffo.getNumOfAddedLines());
			  	 if(linesRemoved.containsKey(type))
			  		linesRemoved.put(type,linesRemoved.get(type)+diffo.getNumOfDeletedLines());
			  	 else
			  		linesRemoved.put(type,diffo.getNumOfDeletedLines());
			  	
			  	 }
	         	pw.println(linesAdded.toString());
	         	pw.println(linesRemoved.toString());
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return employeeID;
	   }

}
