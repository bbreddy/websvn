package org.cigital.websvn.velocity;

	import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

public class FusionChartForDeleted {		
		public String generateJSON(TreeMap<String, TreeMap<String, Integer>> map) {
			String header = "{\n"
					+"\"chart\": {\n"
						+"\t\"caption\": \"Diff\",\n"
						+"\t\"numberprefix\": \"\",\n"
						+"\t\"xaxisname\":\"DATES\",\n"
						+"\t\"yaxisname\":\"LOCC\",\n"
						+"\t\"plotgradientcolor\": \"\",\n"
						+"\t\"bgcolor\": \"FFFFFF\",\n"
						+"\t\"showalternatehgridcolor\": \"0\",\n"
						+"\t\"divlinecolor\": \"CCCCCC\",\n"
						+"\t\"showvalues\": \"0\",\n"
						+"\t\"showcanvasborder\": \"0\",\n"
						+"\t\"canvasborderalpha\": \"0\",\n"
						+"\t\"canvasbordercolor\": \"CCCCCC\",\n"
						+"\t\"canvasborderthickness\": \"1\",\n"
					//	+"\t\"yaxismaxvalue\": \",\n"
						+"\t\"captionpadding\": \"30\",\n"
						+"\t\"yaxisvaluespadding\": \"15\",\n"
						+"\t\"legendshadow\": \"0\",\n"
						+"\t\"legendborderalpha\": \"0\",\n"
						//+"\t\"palettecolors\": \"#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78\",\n"
						+"\t\"showplotborder\": \"0\",\n"
						+"\t\"showborder\": \"0\"\n"
					+"},\n";
				String categories = "\t\"categories\": [\n"
										+"\t\t{\n"
										+"\t\t\"category\": [\n";
				ArrayList<String> date=new ArrayList<String>();
				date.addAll(map.keySet());
				System.out.println(date);
				System.out.println(map);
				for(int i=0;i<date.size();i++)
				{
					//int p;
					//p=filePath.get(i).lastIndexOf("/");

					if(i+1==date.size())
						categories+="\t\t\t{\"label\": \""+date.get(i)+ "\"}\n";
					else

						categories+="\t\t\t{\"label\": \""+date.get(i)+"<br>"+"\"},\n";
					}
				TreeMap<String,ArrayList<Integer>> all=new TreeMap<String, ArrayList<Integer>>();
				for(int i=0;i<date.size();i++)
				{
				TreeMap<String, Integer> now=map.get(date.get(i));
				Iterator<String> vals=now.keySet().iterator();
					while(vals.hasNext()){
						String v=vals.next();
						if(all.containsKey(v)){
							all.get(v).add(now.get(v));
						}
						else{
							ArrayList<Integer> v1=new ArrayList<Integer>();
							v1.add(now.get(v));
							all.put(v, v1);
							}
					}
				}
				
				categories+="\t\t\t]\n\t\t}\n\t],\n"
						//+"\t\"dataset\":[\n"+"\t\t{\n";
						+"\t\"dataset\":[\n"
						+"\t\t{\n";
				
				System.out.println(all);
				Iterator<String> fileTypes=all.keySet().iterator();
				while(fileTypes.hasNext()){
					String vvv=fileTypes.next();
					Iterator<Integer> count=all.get(vvv).iterator();
						categories+="\t\t\t\"seriesname\": \"" +vvv+"\",\n"
							+"\t\t\t\"data\": [\n";
						while(count.hasNext()){
							categories+="\t\t\t{\"value\": \""+count.next()+"\"},\n";
						}
						//categories+="\t\t\t\n]\n\t\t\n}\t\n]\n}";
						if(fileTypes.hasNext())
						categories+="\t\t]\t},\n"+"{";

					}
				categories+="\t\t\t\n]\n\t\t\n}\t\n]\n}";

				return (header+categories);
		}
		}