/**
Author: vinod.K
*/
/**
 * 
 */
package org.cigital.websvn.velocity;

import java.io.PrintWriter;
import java.sql.Date;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.cigital.websvn.model.SvnObject;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author vinod
 *
 */
public class AvgAdded {
	String username,fromdate,todate,rtype;
	PrintWriter pw;
	public int dayc;
	public String result="";
	SessionFactory factory;
	HashMap<String, Integer> linesAdded=new HashMap<String, Integer>();
	HashMap<String, Integer> linesRemoved=new HashMap<String, Integer>();
	public AvgAdded(String username,String fromdate,String todate){
		this.username=username;
		this.fromdate=fromdate;
		this.todate=todate;
		added();
	}
	public int max=0;
	public String added(){
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		String[] fd1=fromdate.split("/");
		System.out.println(fromdate);
		fromdate=fd1[2]+fd1[0]+fd1[1];
		Date d1=new Date(Integer.parseInt(fd1[2]),Integer.parseInt(fd1[0]),Integer.parseInt(fd1[1]));
		fd1=todate.split("/");
		todate=fd1[2]+fd1[0]+fd1[1];
		Date d2=new Date(Integer.parseInt(fd1[2]),Integer.parseInt(fd1[0]),Integer.parseInt(fd1[1]));
		dayc=daysBetween(d1,d2);
		factory=new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
	      Transaction tx = null;
	      Integer employeeID = null;
	      try{
	         tx = session.beginTransaction();
	         List employees;
	         if(username.length()>0)
	        	 employees = session.createQuery("FROM SvnObject R WHERE R.user='"+username+"' and R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
	         else
	        	 employees = session.createQuery("FROM SvnObject R WHERE R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
	         for (Iterator iterator = 
                   employees.iterator(); iterator.hasNext();){
			  	 SvnObject diffo = (SvnObject) iterator.next();
			  	String type=diffo.getFilepath().substring(diffo.getFilepath().lastIndexOf('.')+1);
			  	 if(linesAdded.containsKey(type))
			  		 linesAdded.put(type,linesAdded.get(type)+diffo.getNumOfAddedLines());
			  	 else
			  		linesAdded.put(type,diffo.getNumOfAddedLines());
			  	 if(linesRemoved.containsKey(type))
			  		linesRemoved.put(type,linesRemoved.get(type)+diffo.getNumOfDeletedLines());
			  	 else
			  		linesRemoved.put(type,diffo.getNumOfDeletedLines());
			  	 }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      Collection<Integer> gdg=linesAdded.values();
	      Iterator itr=gdg.iterator();
	      while(itr.hasNext()){
	    	  int ty=(Integer) itr.next();
	    	  if(ty>max)
	    		  max=ty;
	      }
	      gdg=linesRemoved.values();
	      itr=gdg.iterator();
	      while(itr.hasNext()){
	    	  int ty=(Integer) itr.next();
	    	  if(ty>max)
	    		  max=ty;
	      }
	      Iterator<String> itr1=(Iterator<String>) linesAdded.keySet().iterator();
	      while(itr1.hasNext()){
	    	  String tt=itr1.next();
	    	  int cc=linesAdded.get(tt);
	    	  result+=("{\"label\":\""+tt+"\",");
	    	  result+=("\"value\":\""+cc/dayc+"\"},");
	    	  
	      }
	      System.out.println("hello"+result.substring(0,result.length()));
	      result=result.substring(0,result.length());
	      return result;
	}
	public String removed(){
		String result="";
		 Iterator<String> itr=(Iterator<String>) linesRemoved.keySet().iterator();
	      while(itr.hasNext()){
	    	  String tt=itr.next();
	    	  int cc=linesRemoved.get(tt);
	    	  result+=("{\"label\":\""+tt+"\",");
	    	  result+=("\"value\":\""+cc/dayc+"\"},");
	      }
	      return result.substring(0,result.length());
	}
	public int daysBetween(Date d1, Date d2){
		 return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
		}
}
